# IMAGE : registry.gitlab.com/s3w/enviroment:guuao-latest

FROM php:7.3.23-apache-buster
MAINTAINER Victor Serrano <victor.serrano@grupoleiros.com>

RUN apt update && apt install -y gnupg && \
  echo 'deb http://s3-eu-west-1.amazonaws.com/tideways/packages debian main' > /etc/apt/sources.list.d/tideways.list && \
  curl -sS 'https://s3-eu-west-1.amazonaws.com/tideways/packages/EEB5E8F4.gpg' | apt-key add - && \
  apt update && apt install --no-install-recommends -yq \
  sudo \
  jpegoptim \
  optipng \
  telnet \
  cron \
  gnupg \
  gzip \
  libfreetype6-dev \
  libicu-dev \
  libjpeg62-turbo-dev \
  libmcrypt-dev \
  libpng-dev \
  libxslt1-dev \
  lsof \
  default-mysql-client \
  nano \
  libpcre3-dev \
  libzip-dev \
  nfs-common \
  zip \
  tideways-php \
  && docker-php-ext-configure \
  gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-install \
  gd \
  soap \
  opcache \
  intl \
  pdo_mysql \
  mysqli \
  bcmath \
  xsl \
  zip \
  && pecl install redis \
  && docker-php-ext-enable \
  redis \
  tideways\
  && a2enmod rewrite \
  && a2enmod headers \
  && apt-get autoremove --assume-yes && \
     apt-get clean && \
     rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* rm -rf /var/cache/apt/lists/
ARG CACHEBUST=1
#replaces php entrypoint

COPY php.dev.ini "$PHP_INI_DIR/php.ini"
COPY crontab /etc/cron.d/tasks
# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/tasks && /usr/bin/crontab /etc/cron.d/tasks
